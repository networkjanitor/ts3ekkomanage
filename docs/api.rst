API
===

This page describes the exposed methods you can access via http requests. These are mainly used by the ts3ekkoclient module to communicate with this manager instance and therefor are not suited for manual usage.

The webserver runs per default on port 8180.

``/cmd/spawn/<channel_id>``

Spawns a client in the channel identified by the channel id. If there are no client instances left on a server, this can be manually triggered, to re-spawn an instance.

``/cmd/spawn/<channel_id>/<channel_password>``

Spawns a client in the password protected channel identified by the channel id.

``/cmd/despawn/<ekko_node_id>``

Despawns (kills) the client instance identified by the node id.