ts3ekkomanage
=============

This is the management module for ts3ekko. It is responsible mainly for spawning new ts3ekkoclient containers and
despawning old, unwanted ts3ekkoclient containers.

related projects
----------------

- https://gitlab.com/networkjanitor/vagrant-ts3ekko-deploy
- https://gitlab.com/networkjanitor/ts3ekkoutil
- https://gitlab.com/networkjanitor/ts3ekkoclient