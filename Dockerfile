# use debian buster because of python3.6 not being in stretch
FROM debian:buster

# use the same installations steps as in the host provisioning

RUN apt update && apt -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    gnupg

RUN apt-key adv \
    --keyserver \
    hkp://p80.pool.sks-keyservers.net:80 \
    --recv-keys \
    58118E89F3A912897C070ADBF76221572C52609D

# Eh, docker didn't set up buster repo yet. Fortunately the stretch install works just fine on buster.
# FIXME: change to buster repo once released
RUN apt-add-repository 'deb https://apt.dockerproject.org/repo debian-stretch main'


RUN apt update && apt -y install \
    docker \
    python3.6 \
    python3-pip \
    python3-psycopg2

# Set global python3 version to 3.6 instead of 3.5
# FIXME: venv would be better here, but that would require more work
RUN rm /usr/bin/python3 && ln -s /usr/bin/python3.6 /usr/bin/python3

# Install manage application
RUN pip3 install ts3ekkomanage==0.0.4

# Switch to "home" directoy for ease of use
WORKDIR /root/

ENTRYPOINT ["ts3ekkomanage"]